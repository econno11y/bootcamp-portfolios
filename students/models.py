from django.db import models


# Create your models here.
class Student(models.Model):
    name = models.CharField(max_length=250)
    skills = models.TextField()
    image = models.URLField(null=True, blank=True)
    excerpt = models.TextField()
    education = models.CharField(max_length=250)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
